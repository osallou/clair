import ConfigParser
import logging
import logging.config
import os

from clair.clair import Clair

config_file = "config.ini"
if "BIOSHADOCK_CLAIR_CONFIG" in os.environ:
    config_file = os.environ["BIOSHADOCK_CLAIR_CONFIG"]

config = ConfigParser.ConfigParser()
config.readfp(open(config_file))
logging.config.fileConfig(config_file)
log = logging.getLogger(__name__)

cfg = {
    'clair.host': config.get('app:main', 'clair.host'),
    'docker.connect': config.get('app:main', 'docker_connect')
}

test = Clair(cfg)
layers = test.analyse('docker-registry.genouest.org/osallou/testgit')

layer_ids = []
for layer in layers:
    layer_ids.append(layer['id'])
vulnerabilities = test.get_layers_vulnerabilities(layer_ids)

log.warn(str(vulnerabilities))
